package content;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShortTerm extends Deposit {

    private final int INTEREST_RATE = 10;

    public ShortTerm() {
    }

    public ShortTerm(BigDecimal customerNumber, String depositType, BigDecimal depositBalance, BigDecimal durationInDays) {
        super(customerNumber, depositType, depositBalance, durationInDays);
    }

    @Override
    public BigDecimal calculatePayedInterest() {
        return BigDecimal.valueOf(INTEREST_RATE)
                .multiply(super.getDepositBalance())
                .multiply(super.getDurationInDays())
                .divide(BigDecimal.valueOf(36500), 0, RoundingMode.HALF_UP);
    }

    public int getINTEREST_RATE() {
        return INTEREST_RATE;
    }
}
