package content;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongTerm extends Deposit {

    public final int INTEREST_RATE = 20;

    public LongTerm() {
    }

    public LongTerm(BigDecimal customerNumber, String depositType, BigDecimal depositBalance, BigDecimal durationInDays) {
        super(customerNumber, depositType, depositBalance, durationInDays);
    }

    @Override
    public BigDecimal calculatePayedInterest() {
        return BigDecimal.valueOf(INTEREST_RATE)
                .multiply(super.getDepositBalance())
                .multiply(super.getDurationInDays())
                .divide(BigDecimal.valueOf(36500), 0, RoundingMode.HALF_UP);
    }

    public int getINTEREST_RATE() {
        return INTEREST_RATE;
    }
}
