package content;

import java.math.BigDecimal;

public class Deposit {

    private BigDecimal customerNumber;
    private String depositType;
    private BigDecimal depositBalance;
    private BigDecimal durationInDays;

    public Deposit() {
    }

    public Deposit(BigDecimal customerNumber, String depositType, BigDecimal depositBalance, BigDecimal durationInDays) {
        this.customerNumber = customerNumber;
        this.depositType = depositType;
        this.depositBalance = depositBalance;
        this.durationInDays = durationInDays;
    }

    public BigDecimal calculatePayedInterest() {
        return BigDecimal.ZERO;
    }

    public BigDecimal getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(BigDecimal customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
    }

    public BigDecimal getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(BigDecimal durationInDays) {
        this.durationInDays = durationInDays;
    }

        public String toString() {
        return ("Customer Number: " + customerNumber
                + " | Deposit Type: " + depositType
                + " | Deposit Balance: " + depositBalance
                + " | Duration: " + durationInDays
                + " | and Interest of: " + calculatePayedInterest());
    }

//    public void validator() {
//        if (depositType.equals(""))
//    }
}
