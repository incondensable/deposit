package content;

import java.math.BigDecimal;

public class Qarz extends Deposit {

    public Qarz() { }

    public Qarz(BigDecimal customerNumber, String depositType, BigDecimal depositBalance, BigDecimal durationInDays) {
        super(customerNumber, depositType, depositBalance, durationInDays);
    }

    @Override
    public BigDecimal calculatePayedInterest() {
        return BigDecimal.ZERO;
    }
}
