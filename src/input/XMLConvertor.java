package input;

import content.Deposit;
import content.LongTerm;
import content.Qarz;
import content.ShortTerm;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class XMLConvertor implements Serializable {
    public static List<Deposit> getXMLDocument() {
        List<Deposit> deposits = new ArrayList<>();
        Deposit deposit = null;
        List<BigDecimal> interests = new ArrayList<>();

        try {
            File inputFile = new File("D:\\Deposits.txt");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nodeListItems = doc.getElementsByTagName("deposit");

            Class<?> cl = null;

            for (int temp = 0; temp < nodeListItems.getLength(); temp++) {
                Node node = nodeListItems.item(temp);
                Element element = (Element) node;

                String depositType = element.getElementsByTagName("depositType").item(0).getTextContent();

                if (depositType.equalsIgnoreCase("shortTerm")) {
                    cl = ShortTerm.class;
                    deposit = new ShortTerm();
                } else if (depositType.equalsIgnoreCase("longTerm")) {
                    cl = LongTerm.class;
                    deposit = new LongTerm();
                } else if (depositType.equalsIgnoreCase("qarz")) {
                    cl = Qarz.class;
                    deposit = new Qarz();
                } else {

                    break;
                }

                for (Method m : getAllMethods(cl)) {
                    switch (m.getName()) {
                        case "setCustomerNumber":
                            m.invoke(deposit, new BigDecimal(element.getElementsByTagName("customerNumber").item(0).getTextContent()));
                            break;
                        case "setDepositType":
                            m.invoke(deposit, element.getElementsByTagName("depositType").item(0).getTextContent());
                            break;
                        case "setDepositBalance":
                            m.invoke(deposit, new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent()));
                            break;
                        case "setDurationInDays":
                            m.invoke(deposit, new BigDecimal(element.getElementsByTagName("durationInDays").item(0).getTextContent()));
                            break;
                    }
                }

                BigDecimal interest = deposit.calculatePayedInterest();
                interests.add(interest);

                deposits.add(deposit);
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        deposits.sort(Comparator.comparing(Deposit::calculatePayedInterest).reversed());
        return deposits;
    }

    private static Method[] getAllMethods(Class<?> cl) throws ClassNotFoundException, NoSuchMethodException {
        cl = Class.forName("content.Deposit");

        Method setCustomerNumber = cl.getDeclaredMethod("setCustomerNumber", BigDecimal.class);
        Method setDepositType = cl.getDeclaredMethod("setDepositType", String.class);
        Method setDepositBalance = cl.getDeclaredMethod("setDepositBalance", BigDecimal.class);
        Method setDurationInDays = cl.getDeclaredMethod("setDurationInDays", BigDecimal.class);

        return new Method[]{setCustomerNumber, setDepositType, setDepositBalance, setDurationInDays};
    }
}