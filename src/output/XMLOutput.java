package output;

import input.XMLConvertor;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

public class XMLOutput implements Serializable {
    public static void outputting() throws IOException {
        TreeMap<BigDecimal, BigDecimal> result = new TreeMap<>(Collections.reverseOrder());
        StringBuilder sb = new StringBuilder();
        FileWriter fileWriter = new FileWriter("D:\\output.txt");

        for (int i = XMLConvertor.getXMLDocument().size() - 1; i >= 0 ; i--) {
            result.put(XMLConvertor.getXMLDocument().get(i).calculatePayedInterest(), XMLConvertor.getXMLDocument().get(i).getCustomerNumber());
        }

        for (Map.Entry<BigDecimal, BigDecimal> entry : result.entrySet()) {
            sb.append("Customer Number is: " + entry.getValue()).append(" and the interest payed is: " + entry.getKey() + "\n");
        }

        fileWriter.write(String.valueOf(sb));
        fileWriter.close();
    }
}